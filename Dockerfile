FROM node:18

RUN apt-get update && apt-get install -y zip
RUN corepack enable && yarn set version stable && npm install -g npm@9.5.1 typescript@5.1.6
